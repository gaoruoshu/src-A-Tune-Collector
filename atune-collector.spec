%define debug_package %{nil}

Name: atune-collector
Version: 1.1.0
Release: 6
Summary: A-Tune-Collector is used to collect various system resources.
License: Mulan PSL v2
URL: https://gitee.com/openeuler/A-Tune-Collector
Source: https://gitee.com/openeuler/A-Tune-Collector/repository/archive/v%{version}.tar.gz

Patch1: Ubuntu-20.04.3-LTS.patch
Patch2: README-add-data-collecting-output-description.patch
Patch3: fix-bug-list-index-out-of-range-in-virtualbox.patch
Patch4: add-new-dims.patch
Patch5: fix-CWE-23.patch
Patch6: memory-bandwidth-dimm-slot-atuned.patch
Patch7: feature-set-mysql.patch
Patch8: feature-set-redis-value.patch
Patch9: feature-set-nginx.patch
Patch10: bugfix-create-file-only-if-setting-params.patch
Patch11: change-import-behavior.patch
Patch12: atune-collector-add-backup-for-apps.patch
Patch13: feature-add-network-CPI.patch
Patch14: feature-add-multi-for-rps-xps.patch
Patch15: feature-add-rfs-to-network.patch

BuildRequires:  python3-setuptools
Requires:       python3-dict2xml

%description
The A-Tune-Collector is used to collect various system resources and can also be used as the collector of the A-Tune project.

%prep
%autosetup -n A-Tune-Collector-v%{version} -p1

%build
%{py3_build}

%install
%{py3_install}

%check

%files
%license LICENSE
%{python3_sitelib}/atune_*
%attr(0600,root,root) %{_sysconfdir}/atune_collector/*

%changelog
* Wed Nov 29 2023 gaoruoshu <gaoruoshu@huawei.com> - 1.1.0-6
- feature: add rfs and rps/xps=multi to [network]

* Tue Aug 22 2023 gaoruoshu <gaoruoshu@huawei.com> - 1.1.0-5
- feature: add [network] to CPI

* Wed Aug 16 2023 gaoruoshu <gaoruoshu@huawei.com> - 1.1.0-4
- atune-collector: add backup for apps

* Tue Aug 01 2023 gaoruoshu <gaoruoshu@huawei.com> - 1.1.0-3
- feature: enable application config

* Thu Mar 09 2023 zhangjian <zhang_jian7@hoperun.com> - 1.1.0-2
- fix bug: list index out of range in virtualbox

* Thu Nov 11 2021 hanxinke <hanxinke@huawei.com> - 1.1.0-1
- upgrade to v1.1.0

* Wed Feb 10 2021 hanxinke <hanxinke@huawei.com> - 1.0-1
- Package init
